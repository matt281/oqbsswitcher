# http://clarkgrubb.com/makefile-style-guide

MAKEFLAGS += --warn-undefined-variables --no-print-directory
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := install
.DELETE_ON_ERROR:
.SUFFIXES:

.PHONY: install
install:
	@sudo install bs_backup /usr/local/bin
	@sudo install bs_restore /usr/local/bin
	@echo "Success!"
