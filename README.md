# oqbsswitcher

## Installation for MacOS/Linux

```
git clone https://gitlab.com/matt281/oqbsswitcher.git
cd oqbsswitcher
sudo ln -sf bs_backup /usr/local/bin
sudo ln -sf bs_restore /usr/local/bin
```

## Upgrading
```
cd oqbsswitcher
git pull
```

## Usage

Make sure adb connected:

```
adb devices
...(should show a device)
```

Create two directories, one for modded backups, one for unmodded. E.g.:

```
mkdir Modded
mkdir Unmodded
```

To switch from modded to unmodded:

```
cd Modded
bs_backup
cd ../Unmodded
bs_restore
```

To switch from unmodded to modded:

```
cd Unmodded
bs_backup
cd ../Modded
bs_restore
```

Always remember to backup first, and make sure not to overwrite the modded
backup with unmodded data, or vice versa. There is some detection for
this, which will prompt you if it looks like you're about to overwrite
your modded backup with unmodded data and vice versa but be careful
nonetheless.

If unsure, create a brand new directory and do `bs_backup` before doing
anything else.
